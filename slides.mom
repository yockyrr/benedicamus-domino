.TITLE "Organal motets, organum prosulae, polyphonic tropes, motet-likes: the \
universalisation of the motet"
.AUTHOR "Joshua Stutter"
.PDF_TITLE "\*[$TITLE]"
.DOCTYPE SLIDES \
ASPECT 16:9 \
HEADER \
"\*[$AUTHOR] (University of Glasgow)" \
"\f[B]The universalisation of the motet\f[R]" \
"\*[SLIDE#]/17"
.PT_SIZE 16
.XCOLOR black
.UNDERSCORE_WEIGHT 2.5
.AUTOLEAD 1.25 FACTOR
.B_MARGIN 1p
.FOOTER_MARGIN 0
.char \[idem] \v'-.3m'\l'3m'\v'.3m'
.START
.EQ
delim $$
.EN
.HEADING 1 "The universalisation of the motet"
.PP
.BLOCKQUOTE
`In its long history, the motet touches on nearly every aspect of sacred and
.br
secular musical culture'
.QUAD R
\(em Haar, \(lqConference Introductory Remarks\(rq in \f[I]Hearing the
Motet\f[R], p.12
.BLOCKQUOTE OFF
.JUSTIFY
.HEADING 2 "The usual story \[u2026]"
.PP
.LIST BULLET
.ITEM
Conductus
.ITEM
Organum purum
.ITEM
Purum \[->] Discantus
.ITEM
Clausulae
.ITEM
Motet
.LIST OFF
.sp 1v
\s-5\[dg] By \f[I]organum\f[R], I mean all \f[I]purum\f[R] and
\f[I]discantus\f[R].  By \f[I]discantus\f[R] I refer to passages with measured
tenors.
.br
By \f[I]clausulae\f[R], I mean only so-called \(lqsubstitute\(rq clausulae.\s+5
.NEWSLIDE
.HEADING 1 "My focus"
.LIST BULLET
.ITEM
Historiographical perspective
.ITEM
Etiology not etymology
.LIST OFF
.sp 1v
.HEADING 1 "A typical definition of \(lqmotet\(rq"
.PP
An early motet is a self-contained piece of polyphony, with all parts in
\f[I]discantus\f[R], derived from
.br
liturgical polyphony, and with an added text that is broadly syllabic.
.sp 1v
.HEADING 1 "Problem"
.PP
This definition causes the distinction between the term \(lqmotet\(rq,
\(lqorganal motet\(rq, \(lqtrope\(rq, \(lqprosula\(rq, and \(lqrefrain\(rq to
become arbitrary.
.NEWSLIDE
.TS
center tab(;) linesize(32p);
lb  lb  lb  lb
lxi lxi l   l .
Incipit;Tenor;Source;Folio
_
Stirps yesse;Benedicamus domino;F-Pnm Lat.\ 1139;f.60v\(en61r [1\*[SUP]st\*[SUPX] v.]
;;F-Pnm Lat.\ 3549;f.166v\(en167r [2\*[SUP]nd\*[SUPX] v.]
Humane prolis;Benedicamus domino;F-Pnm Lat.\ 3719;f.70r
Trino domino;Benedicamus domino;F-Pnm Lat.\ 3719;f.70v
Amborum sacrum;Benedicamus domino;GB-Cu Ff.i.17;f.4v, 3v
Vidit rex omnipotens;Viderunt omnes;A-Iu 457;f.79v\(en80v
Beatis nos adhibe;Benedicamus domino;I-Fl Plut.\ 29.1;f.250r\(en252r
Veni doctor previe;Veni sancte spiritus;I-Fl Plut.\ 29.1;f.390v\(en392v
;;GB-Lbl Egerton 2615;f.69v\(en71v
;;;f.84v\(en86r
Vide prophetie;[Viderunt];D-W Cod.\ Guelf.\ 1099 Helmst.;f.167r\(en168r
;;PL-Stk Muz 9;f.7v [fragmentary]
;;CZ-Pak N VIII;f.37v [text only]
;;D-DS 521;f.220v [text only]
;;[E-Mn 20486];[lost before f.5 ?]
.TE
.NEWSLIDE
.TS
center tab(;) linesize(32p);
lb  lb  lb  lb
lxi lxi l   l .
Incipit;Tenor;Source;Folio
_
Homo cum mandato;[Omnes];D-W Cod.\ Guelf.\ 1099 Helmst.;f.168r\(en168v
;;CZ-Pak N VIII;f.37v [text only]
;;D-DS 521;f.220v [text only]
;;[E-Mn 20486];[lost before f.5 ?]
De stephani roseo;[Sederunt];D-W Cod.\ Guelf.\ 1099 Helmst.;f.168v\(en170r
;;E-Mn 20486;f.2v\(en35 [ending only]
;;A-Gu 756;f.185r\(en185v
;;CZ-Pak N VIII;f.37v [text only]
Adesse festina;[Adiuva me [etc.]];D-W Cod.\ Guelf.\ 1099 Helmst.;f.170r\(en173r
;;E-Mn 20486;f.5v\(en13v
;;PL-Stk Muz 9;[fragmentary]
;;CZ-Pak N VIII;f.37v [text only]
Associa tecum;[Sancte germane];I-Fl Plut.\ 29.1;f.450r\(en450v
;;CZ-Pak N VIII;f.37v [text only]
.TE
.PP
Also: \f[I]Notum fecit deus mundo \(em Notum fecit dominus\f[R] (A-Iu 457,
f.80v; text: PL-Wn 12722 V, f.30v) \f[B]and\f[R] \f[I]Gaude syon \(em Et
iherusalem\f[R] (I-Fl Plut.\ 29.1, f.410r; D-Mbs Clm.\ 16444, f.VI\(enVIv).
.NEWSLIDE
.BLOCKQUOTE
A song made for the church, either upon some hymne or Antheme, or such like
.QUAD R
\(em Morley, \f[I]A plaine and easie introduction to practicall musicke\f[R],
p.179
.BLOCKQUOTE OFF
.JUSTIFY
.BLOCKQUOTE
A sort of nonsense part [\[u2026]] sung to nonsense syllables, such as
\(lqBalaam,\(rq or \(lqPortare,\(rq or \(lqVerbum,\(rq or \(lqAngelus,\(rq
[\[u2026]] the practice was so well understood that the composer merely wrote
the word once at the beginning of a piece and singers (generally those who took
the lower part) fitted it in as seemed to them good.
.QUAD R
.sp -1v
\(em Parry \f[I]The art of music\f[R], pp.100\(en101
.BLOCKQUOTE OFF
.JUSTIFY
.BLOCKQUOTE
In the twelfth century, a Frenchman had the idea that the praise of God could be
enriched by using these polyphonic coloraturas [i.e.\ \f[I]clausulae\f[R]] over
the vowels `u' or `a' in such a way that while the lower voice would sing its
old melody with a constant repetition of the vowel `u' or `a', the upper voices
would sing their newly crafted melodies with various other syllables instead of
just `u' or `a', that is, a completely newly composed song text.
.QUAD R
\(em Meyer \f[I]Der Ursprung des Motetts\f[R], p.310 (my translation)
.BLOCKQUOTE OFF
.JUSTIFY
.NEWSLIDE
.PDF_IMAGE -C "Mafol6rI.pdf" 1700p 2149p SCALE 15 CAPTION "E-Mn 20486, f.6r. \
\f[I]Adesse festina\f[R]"
.NEWSLIDE
.PDF_IMAGE -C "Ffol250r390v.pdf" 637p 454p SCALE 67 CAPTION "I-Fl Plut.\ 29.1, \
f.250r (\f[I]Beatis nos adhibe\f[R]) and f.390v (\f[I]Veni doctor previe\f[R])"
.NEWSLIDE
.PDF_IMAGE -C "GBCuFfi17fol4v.pdf" 2266p 2816p SCALE 10 CAPTION "GB-Cu Ff.i.17\
, f.4v. \f[I]Amborum sacrum\f[R]"
.NEWSLIDE
.PDF_IMAGE -C "AIu457fol79v-80v.pdf" 2421p 980p SCALE 29 CAPTION "A-Iu 457, \
f.79v\(en80v. \f[I]Vidit rex omnipotens\f[R] and \f[I]Notum fecit deus \
mundo\f[R]"
.NEWSLIDE
.BLOCKQUOTE
This is \f[I]the first motet\f[R] [emphasis original] [\[u2026]] in fact, the
first pieces of this kind and from this period were not subject to meter:
theorists state this explicitly, such as Anonymous IV published by De
Coussemaker, \f[I]Discantus positio vulgaris\f[R], and much later, the English
monk Walter Odington.
.br
.QUAD R
\(em Gastou\['e], \f[I]Les Primitifs de la Musique Fran\[,c]aise\f[R],
pp.14\(en15 (my translation)
.BLOCKQUOTE OFF
.JUSTIFY
.BLOCKQUOTE
So we see that motets, i.e.\ polyphonic structures with different texts and a
given basic melody, can already be found in a stylistic period that was not
familiar with the large melismatic \f[I]organa\f[R] of Notre Dame with their
sectional compositions.
.br
.QUAD R
\(em Handschin, \f[I]\[:U]ber den Ursprung der Motette\f[R], p.193 (my
translation)
.BLOCKQUOTE OFF
.JUSTIFY
.BLOCKQUOTE
To sum up all the fragments of information gathered from the theorists: the
motet has several texts, as many as there are voice parts; it may consist of a
tenor and \f[I]motetus\f[R], or of these two and a \f[I]triplum\f[R] (as well as
a \f[I]quadruplum\f[R]); the tenor is taken from a pre-existent source and is
counted as a text (Franco).  The tenor is arranged in a modal pattern; the main
beats of all parts must be consonant (\f[I]Discantus positio vulgaris\f[R]).
.br
.QUAD R
\(em Tischler, \f[I]The Motet in Thirteenth Century France\f[R], p.34
.BLOCKQUOTE OFF
.JUSTIFY
.NEWSLIDE
.HEADING 1 "Gaude syon: motet or not?"
.TS
center tab(;) linesize(32p);
rb cubp+5 l cubp+5 lb
r  cubp+5 l cubp+5 l
r  cubp+5 l cubp+5 l
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
r  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^
^  cubp+5 l cubp+5 ^ .
I-Fl Plut.\ 29.1;;;;D-Mbs Clm.\ 16444

\f[I]Discantus\f[R];;Gaude syon filia;;\f[I]Discantus
;\[lt];regis in presentia;\[rt];
;\[braceleftex];cuius gratia;\[bracerightex];
;\[lk];prestans ducem;\[bracerightex];
;\[lb];cecis fundens lucem;\[bracerightex];
Organal;;te refecit;\[bracerightex];
;\[lt];et prefecit;\[rk]
;\[braceleftex];firma sta constantia;\[bracerightex];
;\[braceleftex];cesset metus;\[bracerightex];
;\[lk];languor fletus;\[bracerightex];
;\[braceleftex];splendor te perlustrat glorie;\[bracerightex];
;\[braceleftex];uiam pandit patrie;\[bracerightex];
;\[lb];sol iustitiae;\[rb];
.TE
.NEWSLIDE
.HEADING 1 "Taxonomy after Payne (1991)"
.TS
center tab(;) linesize(32p);
lb lb lb
li li li .
Organum trope;Organum prosula;Motet
_
Stirps yesse;Vide prophetie;[Texted \f[R]discantus clausulae\f[I]]
Humane prolis;Homo cum mandato;
Trino domino;De stephani roseo;
Amborum sacrum;Adesse festina;
Vidit rex omnipotens;Associa tecum;
Beatis nos adhibe;;
Veni doctor previe;;
.TE
.PP
\f[I]Gaude syon filia\f[R] is a \[u2026] prosula? motet?
.PP
\f[I]Notum fecit deus mundo\f[R] is \[u2026] neither? \f[I]conductus\f[R]?
.sp 1v
.PP
The term \(lqConductus-motet\(rq was coined by Husmann (\f[I]Die drei- und
vierstimmigen\f[R]), \(lqRefrain cento\(rq by R.\ Meyer (see Everist, \f[I]The
Refrain Cento\f[R], p.164), and \(lqExtensio modi\(rq by Apel (see Everist
\f[I]Discovering Medieval Song\f[R], p.96).
.NEWSLIDE
.BLOCKQUOTE
Three derivations are linguistically justifiable.
.QUAD R
.sp -1v
\(em Dammann, \f[I]Geschichte der
.br
Begriffsbestimmung Motette\f[R], p.338 (my translation)
.BLOCKQUOTE OFF
.JUSTIFY
.BLOCKQUOTE
The Latin musical term \(lqmotetus\(rq was consciously reinterpreted and
apocryphally re-etymologised in ignorance of its vernacular roots, as well as
consciously reshaped, according to the central intention of the respective
author and the specific aspect of the motet he wanted to emphasise as eponymous.
.QUAD R
\(em Beiche, \f[I]Motette\f[R], p.3 (my translation)
.BLOCKQUOTE OFF
.JUSTIFY
.BLOCKQUOTE
It is probably not possible to draw any sustainable conclusions about the
history of the term.
.QUAD R
.sp -1v
\(em Hofmann, \f[I]Zur Entstehungs- und Fr\[:u]hgeschichte
.br
des Terminus Motette\f[R], p.144 (my translation)
.BLOCKQUOTE OFF
.JUSTIFY
.BLOCKQUOTE
Before the application of the term \(lqmotet\(rq was universalized, the
earliest examples of what we would now consider Latin motets are labelled as
tropes or prosulae in the sources, if they have any designation at all.
.QUAD R
\(em Leach, \f[I]The Genre(s) of Medieval Motets\f[R], p.17
.BLOCKQUOTE OFF
.JUSTIFY
.NEWSLIDE
.HEADING 1 "Thank you"
.CENTER
Slides available at: <https://yockyrr.gitlab.io/benedicamus-domino/>
.vs -.5v
.PDF_IMAGE -C "qrcode.pdf" 2496p 2496p SCALE 6
.HEADING 1 "References \s-8(continued on next slides)\s+8"
.QUAD L
.PT_SIZE -4
.AUTOLEAD -1
.in +2mu
.ti -2mu
Aubry, Pierre. \(lqIter Hispanicum: Notices et extraits de manuscrits de musique
ancienne conserv\['e]s dans les biblioth\[`e]ques d'Espagne.\(rq
\f[I]Sammelb\[:a]nde der Internationalen Musikgesellschaft.\f[R] 8 3 (1907) :
337\(en355 <https://www.jstor.org/stable/929132>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Beiche, Michael. \(lqMotette.\(rq \f[I]Handw\[:o]rterbuch Der Musikalischen
Terminologie.\f[R] Ed.\ Eggebrecht, Hans Heinrich. 4. Stuttgart: Franz Steiner,
2004.
.in
.ti
.BR
.in +2mu
.ti -2mu
Bradley, Catherine A. \(lqContrafacta and Transcribed Motets: Vernacular
Influences on Latin Motets and Clausulae in the Florence Manuscript.\(rq
\f[I]Early Music History\f[R] 32 (2013) : 1\(en70
<https://doi.org/10.1017/S0261127913000016>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Bukofzer, Manfred. \(lqInterrelations between Conductus and Clausula.\(rq
\f[I]Annales musicologiques\f[R] 1 (1953) : 65\(en103
.in
.ti
.BR
.in +2mu
.ti -2mu
de Coussemaker, Edmond. \f[I]L'art Harmonique aux XII\*[SUP]e\*[SUPX] et
XIII\*[SUP]e\*[SUPX] Si\[`e]cles\f[R]. Paris: A. Durand, Libraire, 1865.
.in
.ti
.BR
.in +2mu
.ti -2mu
Dammann, Rolf. \(lqGeschichte der Begriffsbestimmung Motette.\(rq \f[I]Archiv
f\[:u]r Musikwissenschaft\f[R] 16 4 (1959) : 337\(en377
<https://doi.org/10.2307/930075>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Desmond, Karen. \(lqW.\ de Wicumbe's Rolls and Singing the Alleluya ca.\
1250.\(rq \f[I]Journal of the American Musicological Society\f[R] 73 3 (2020) :
639\(en709 <https://doi.org/10.1525/jams.2020.73.3.639>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Everist, Mark. \f[I]Discovering Medieval Song: Latin Poetry and Music in the
Conductus\f[R]. Cambridge: Cambridge University Press, 2018
<https://doi.org/10.1017/9780511852138>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Falck, Robert. \f[I]The Notre Dame Conductus: A Study of the Repertory\f[R].
Ottawa: Institute of Mediaeval Music, 1981.
.in
.ti
.BR
.in +2mu
.ti -2mu
F\['e]tis, Fran\[,c]ois-Joseph. \f[I]Histoire G\['e]n\['e]rale de la
Musique.\f[R] Paris: Librairie de Firmin Didot Fr\[`e]res, Fils et Cie, 1869.
.in
.ti
.BR
.in +2mu
.ti -2mu
Frobenius, Wolf. \(lqZum genetischen Verh\[:a]ltnis zwischen Notre-Dame-Klauseln
und ihren Motetten.\(rq \f[I]Archiv f\[:u]r Musikwissenschaft\f[R] 44 1 (1987) :
1\(en39 <https://doi.org/10.2307/930547>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Fuller, Sarah. \f[I]Aquitanian Polyphony of the Eleventh and Twelfth
Centuries.\f[R] PhD diss. Berkeley, CA: University of California, 1969.
.in
.ti
.BR
.in +2mu
.ti -2mu
Gastou\['e], Am\['e]d\['e]e. \f[I]Les Primitifs de la Musique
Fran\[,c]aise.\f[R] Paris: Librairie Renouard, 1922.
.in
.ti
.BR
.in +2mu
.ti -2mu
Gillingham, Bryan. \(lqA New Etymology and Etiology for the Conductus.\(rq
\f[I]Beyond the Moon: Festschrift Luther Dittmer\f[R] Eds.\ Gillingham, Bryan
and Paul Merkley. Ottawa: The Institute of Mediaeval Music, 1990. 100\(en117.
.in
.ti
.BR
.in +2mu
.ti -2mu
Haar, James. \(lqConference Introductory Remarks.\(rq \f[I]Hearing the Motet:
Essays on the Motet of the Middle Ages and Renaissance\f[R] Ed.\ Pesce, Dolores.
Oxford: Oxford University Press, 1997. 12\(en16.
.in
.ti
.BR
.in +2mu
.ti -2mu
Handschin, Jacques. \(lq\[:U]ber den Ursprung der Motette.\(rq \f[I]Bericht
\[:u]ber den Musikwissenschaftlichen Kongre\[ss] in Basel 1924.\f[R] Leipzig,
1925. 189\(en200.
.in
.ti
.BR
.in +2mu
.ti -2mu
Hofmann, Klaus. \(lqZur Entstehungs- und Fr\[:u]hgeschichte des Terminus
Motette.\(rq \f[I]Acta Musicologica.\f[R] 42 3/4 (1970) : 138\(en150
<https://doi.org/10.2307/932192>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Husmann, Heinrich. \f[I]Die drei- und vierstimmigen Notre-Dame-Organa.\f[R]
Hildesheim: Georg Olms Verlag, 1989.
.in
.ti
.BR
.in +2mu
.ti -2mu
Leach, Elizabeth Eva. \(lqThe Genre(s) of Medieval Motets.\(rq \f[I]A Critical
Companion to Medieval Motets.\f[R] Ed.\ Hartt, Jared C. Woodbridge: The Boydell
Press, 2018. 15\(en42 <https://doi.org/10.2307/j.ctvc16pzc.11>.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \(lqReview of Saint-Cricq: Motets from the Chansonnier de Noailles.\(rq
\f[I]Music and Letters\f[R] 99 2 (2018) : 281\(en285
<https://doi.org/10.1093/ml/gcy056>.
.in
.ti
.BR
.in +2mu
.ti -2mu
Ludwig, Friedrich. \f[I]Repertorium Organum Recentioris et Motetorum
Vetustissimi Stili.\f[R] Ed.\ Dittmer, Luther A. New York, NY: Institute of
Mediaeval Music, 1910.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \(lqDie Quellen der Motetten \[:a]ltesten Stils.\(rq \f[I]Archiv
f\[:u]r Musikwissenschaft\f[R] 5 3 (1923) : 185\(en222
<https://doi.org/10.2307/929681>.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \(lqDie geistliche nichtliturgische, weltliche einstimmige und die
mehrstimmige Musik des Mittelalters bis zum Anfang des 15.\ Jahrhunderts.\(rq
\f[I]Handbuch der Musikgeschichte.\f[R] 1929. Ed.\ Adler, Guido. 2nd ed. Berlin:
Keller, 1961. 157\(en295.
.in
.ti
.BR
.in +2mu
.ti -2mu
Meyer, Wilhelm. \(lqDer Ursprung des Motetts.\(rq \f[I]Gesammelte Abhandlungen
zur Mittellateinischen Rhythmik. 1898. 2. Berlin: Weidmannsche Buchhandlung,
1905. 303\(en341.
.in
.ti
.BR
.in +2mu
.ti -2mu
Morley, Thomas. \f[I]A Plaine and Easie Introduction to Practicall Musicke, set
downe in forme of a dialogue\f[R]. London: Peter Short, 1597.
.in
.ti
.BR
.in +2mu
.ti -2mu
Parry, C. Hubert H. \f[I]The Art of Music\f[R]. London: Kegan Paul, Trench,
Tr\[:u]bner & Co., 1893.
.in
.ti
.BR
.in +2mu
.ti -2mu
Payne, Thomas B. \(lqAssocia tecum in patria: A Newly Identified Organum Trope
by Philip the Chancellor.\(rq \f[I]Journal of the American Musicological
Society\f[R] 39 2 (1986) : 233\(en254 <https://doi.org/10.2307/831530>.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \f[I]Poetry, Politics, and Polyphony: Philip the Chancellor's
Contribution to the Music of the Notre Dame School\f[R]. PhD diss. Chicago, IL:
University of Chicago, 1991.
.in
.ti
.BR
.in +2mu
.ti -2mu
Reese, Gustave. \f[I]Music in the Middle Ages.\f[R] New York, NY: W.\ W.\ Norton
& Company, 1940.
.in
.ti
.BR
.in +2mu
.ti -2mu
Roesner, Edward. \(lqThe problem of chronology in the transmission of organum
duplum.\(rq \f[I]Music in Medieval and Early Modern Europe: Patronage, Sources
and Texts.\f[R] Ed.\ Fenlon, Iain. Cambridge: Cambridge University Press, 1981.
365\(en399.
.in
.ti
.BR
.in +2mu
.ti -2mu
Rokseth, Yvonne. \f[I]Polyphonies du xiii\*[SUP]e\*[SUPX] si\[`e]cle: Le
manuscrit H196 de la Facult\['e] de m\['e]decine de Montpellier\f[R]. Vol. 4.
Paris: \['E]ditions de l'Oiseau-Lyre, 1939.
.in
.ti
.BR
.in +2mu
.ti -2mu
Sanders, Ernest H. \(lqPeripheral Polyphony of the 13th Century.\(rq
\f[I]Journal of the American Musicological Society\f[R] 17 3 (1964) : 261\(en287
<https://doi.org/10.2307/830091>.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \(lqTonal Aspects of 13th-Century English Polyphony.\(rq \f[I]Acta
Musicologica\f[R] 37 1 (1965) : 19\(en34 <https://doi.org/10.2307/932336>.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \(lqThe Medieval Motet.\(rq \f[I]Gattungen der Musik in
Einzeldarstellungen: Gedenkschrift Leo Schrade.\f[R] Eds.\ Arlt, Wulf, Ernst
Lichtenhahn, Hans Oesch, and Max Haas. Bern: Francke, 1973. 497\(en573.
.in
.ti
.BR
.in +2mu
.ti -2mu
Tischler, Hans. \f[I]The Motet in Thirteenth Century France.\f[R] PhD diss. New
Haven, CT: Yale University, 1942.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \(lqNew Historical Aspects of the Parisian Organa.\(rq
\f[I]Speculum\f[R] 25 1 (1950) : 21\(en35 <https://doi.org/10.2307/2850001>.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \(lqThe Earliest Motets: Origins, Types and Groupings.\(rq \f[I]Music
& Letters\f[R] 60 4 (1979) : 416\(en427 <https://doi.org/10.1093/ml/60.4.416>.
.in
.ti
.BR
.in +2mu
.ti -2mu
\[idem]. \f[I]The Earliest Motets (to Circa 1270): a Complete Comparative
Edition.\f[R] London: Yale University Press, 1982.
.in
.ti
.BR
.in +2mu
.ti -2mu
Yudkin, Jeremy. \f[I]The Music Treatise of Anonymous IV: A New Translation.\f[R]
Neuhausen-Stuttgart: American Institute of Musicology, 1985.
.in
.ti
