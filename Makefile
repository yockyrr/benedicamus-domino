all: slides.pdf
slides.pdf: slides.mom
	pdfmom -etGp slides.mom > slides.pdf
	gs -q -dNOPAUSE -dBATCH -dSAFER -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -dPDFSETTINGS=/prepress -dEmbedAllFonts=true -dSubsetFonts=false -dAutoRotatePages=/None -sOutputFile=slides-tmp.pdf slides.pdf
	mv slides-tmp.pdf slides.pdf
watch:
	inotifywait -mqe close_write slides.mom | while read EV; do \
		jobs -p | xargs kill -9; \
		( \
			make; \
			echo "$EV"; \
		) & \
	done
